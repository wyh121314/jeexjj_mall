<#--
/****************************************************
 * Description: t_mall_address的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>address_id</th>
	        <th>user_id</th>
	        <th>user_name</th>
	        <th>tel</th>
	        <th>street_name</th>
	        <th>is_default</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.addressId}
			</td>
			<td>
			    ${item.userId}
			</td>
			<td>
			    ${item.userName}
			</td>
			<td>
			    ${item.tel}
			</td>
			<td>
			    ${item.streetName}
			</td>
			<td>
			    ${item.isDefault}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/address/input/${item.id}','修改t_mall_address','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/address/delete/${item.id}','删除t_mall_address？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>